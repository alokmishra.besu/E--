#ifndef CODEGEN_H
#define CODEGEN_H

#include <sstream>
#include <string>
#include <vector>
#include <set>
#include "IncludeHeaders.h"

using namespace std;

class Instruction;
class OpCodeInstMap;
class CodeModule;
class VariableEntry;
class OpNode;

// Instructions for Assembly language --------------------------------------------------------------------------------------
class Instruction {
public:
  enum InstructionSet {
    ADD,
    SUB,
    DIV,
    MUL,
    MOD,
    FADD,
    FSUB,
    FDIV,
    FMUL,
    AND,
    OR,
    XOR,
    NEG,
    FNEG,
    UGT,
    UGE,
    GT,
    GE,
    EQ,
    NE,
    FGT,
    FGE,
    FEQ,
    FNE,
    PRTS,
    PRTI,
    PRTF,
    JMP,
    JMPC,
    JMPI,
    JMPCI,
    MOVL,
    MOVS,
    MOVI,
    MOVF,
    MOVIF,
    MOVFI,
    STI,
    STF,
    LDI,
    LDF,
    IN,
    INI,
    INF,
    BLANK,
    DEFAULT
  };

  Instruction(InstructionSet instr, string param1, string param2 = "", string param3 = "", string label = "", string comment = "") {
    instr_ = instr;
    param1_ = param1;
    param2_ = param2;
    param3_ = param3;
    label_ = label;
    comment_ = comment;
  }
  Instruction(string label) {
    instr_ = InstructionSet::BLANK;
    label_ = label;
  }
  ~ Instruction() {}

  // Member variable accessors
  InstructionSet getInstr() const { return instr_; }
  string getParam1() const { return param1_; }
  string getParam2() const { return param2_; }
  string getParam3() const { return param3_; }
  string getLabel() const { return label_; }

  // Member variable mutators
  void setParam1(string param1) { param1_ = param1; }
  void setParam2(string param2) { param2_ = param2; }
  void setParam3(string param3) { param3_ = param3; }
  void setLabel(string label) { label_ = label; }
  void setComment(string comment) { comment_ = comment; }

  string name(InstructionSet inst);
  string toString(bool newLine = true);
  static Instruction* decrSP();

private:
  InstructionSet instr_;
  string param1_;
  string param2_;
  string param3_;
  string label_;
  string comment_;
};

// -------------------------------------------------------------------------------------------------------------------
class OpCodeInstMap {
public:
  OpCodeInstMap(OpNode::OpCode oprCode, initializer_list<Instruction::InstructionSet> list) {
    int i = 0;
    oprCode_ = oprCode;
    for (Instruction::InstructionSet inst : list) {
      instr_[i++] = inst;
    }
    instrSize_ = i;
  }

  static OpCodeInstMap* getOpCode(OpNode::OpCode oprCode) { return opCodeInstMap_[(int) oprCode]; }

  static Instruction::InstructionSet fetchInstr(OpNode::OpCode oprCode, int instNum) {
    OpCodeInstMap *p = getOpCode(oprCode);
    if (instNum < p->instrSize_)
      return p->instr_[instNum];
    else
      return p->instr_[p->instrSize_ - 1];
  }

  static OpCodeInstMap* opCodeInstMap_[31];

private:
  OpNode::OpCode oprCode_;
  Instruction::InstructionSet instr_[10];
  int instrSize_;
};

// -------------------------------------------------------------------------------------------------------------------

/*
* A E-- program can contain many modules. This class will hold them in a vector. This would be used
* as a member variable for the GlobalEntry class
*/
class ModuleList {
public:
  ModuleList(string progName) {
    progName_ = progName;
    modules_ = new vector<CodeModule*>;
  }
  ~ModuleList() {}

  vector<CodeModule*>* getModule() { return modules_; }
  void insertModule(CodeModule* codeMod) { modules_->push_back(codeMod); }

private:
  string progName_;
  vector<CodeModule*> *modules_;
};

// ---------------------------------------------------------------------------------------------------------------------
class CodeModule {
public:
  CodeModule(string moduleName) {
    moduleName_ = moduleName;
    instructions_ = new vector<Instruction*>;
  }

  ~CodeModule() { delete instructions_; }

  vector<Instruction*>* getInstructions() { return instructions_; }

  void insertInstructionSet(Instruction *instr) { if (instr != NULL) instructions_->push_back(instr); }

  void insertInstructionSet(vector<Instruction *> *instrVector) { concatVector(instructions_, instrVector); }

  Instruction* firstInst() const {
    if (instructions_->size()) {
      return instructions_->front();
    }
    return NULL;
  }

private:
  string moduleName_;
  vector<Instruction*> *instructions_ = NULL;
};

// ----------------------------------------------------------------------------------------------------------------------

/*
* implemented for the function invocation. It holds the list of actual parameters to the function
*/
class ActualParamElem: public ProgramElem {
public:
  ActualParamElem(vector<InterVar*>* params) { params_ = params; }

  vector<InterVar*>* getParams() { return params_; }

private:
  vector<InterVar*>* params_;
};

// ----------------------------------------------------------------------------------------------------------------------
class IntrLabel: public ProgramElem {
public:
  IntrLabel(string label) { label_ = label; }

  string getLabel() { return label_; }

private:
  string label_;
};

// Quadruples to hold the intermediate code -------------------------------------------------------------------
class Quadruple: public ProgramElem {
public:
  Quadruple(OpNode::OpCode opc, InterVar *opr1, InterVar *opr2 = NULL, InterVar *res = NULL, string label = "") {
    opc_ = opc;
    opr1_ = opr1;
    opr2_ = opr2;
    res_ = res;
    label_ = label;
  }
  ~ Quadruple() {}

  // Member variable mutators
  void setOpc(OpNode::OpCode opc) { opc_ = opc; }
  void setOpr1(InterVar *opr1) { opr1_ = opr1; }
  void setOpr2(InterVar *opr2) { opr2_ = opr2; }
  void setRes(InterVar *res) { res_ = res; }
  void setLabel(string label) { label_ = label; }

  // Member variable accessors
  OpNode::OpCode getOpc() { return opc_; }
  InterVar* getOpr1() { return opr1_; }
  InterVar* getOpr2() { return opr2_; }
  InterVar* getRes() { return res_; }
  string getLabel() { return label_; }

  string toString(bool newline = true);

  // Convert quadruple to assembly code
  static vector<Instruction*>* iCodeToAsmGen(vector<Quadruple*> *quad, bool showComment = true, bool purgeRegisters = true);
  static int tempCnt_;
  static string fetchTempVar();
  static void resetTempCnt();
  bool isEqual(Quadruple *quad);

private:
  OpNode::OpCode opc_;
  InterVar *opr1_;
  InterVar *opr2_;
  InterVar *res_;
  string label_;
};

class InterVar {
public:
    enum class ElemType {
        VAR_TYPE, TEMP_VAR_TYPE, VAL_TYPE,
        INV_NODE_TYPE, REF_EXPR_TYPE, QUAD_TYPE,
        LABEL_TYPE, PARAM_TYPE
    };

    InterVar(ProgramElem *elem, ElemType type) {
        progElem_ = elem;
        type_ = type;
    };

    ProgramElem* getElem()  {
        return progElem_;
    };
    ElemType getType() {
        return type_;
    };

    bool equals(InterVar *p);
    bool uses(InterVar *p);

    static string toString(InterVar *ice);

private:
    ElemType type_;
    ProgramElem* progElem_;
};
#endif

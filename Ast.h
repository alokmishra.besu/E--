#ifndef AST_H
#define AST_H

#include "all.h"
#include "IncludeHeaders.h"
#include "Value.h"
#include "ProgramElem.h"

class BlockEntry;
class EFSA;
class EventEntry;
class FunctionEntry;
class LabelEntry;
class OpNode;
//enum class OpCode;
class PatNode;
//enum class PatNode;
class PrimitivePatNode;
class RuleNode;
class SymTabEntry;
class VariableEntry;

class Instruction;
class Quadruple;
class InterVar;

/******************************************************************************
 Here is the class hierarchy:
                               ProgramElem
                                    |
                                 AstNode
                                    |
 +----------------+-----------------+----------+--------------+
 |		            |                             |              |
 BasePatNode   ExprNode                      RuleNode      StmtNode
 |                |                                            |
 |                |                                            |
 |      +---------+----------+-----------+                     |
 |      |         |          |           |                     |
 |  RefExprNode  OpNode  ValueNode  InvocationNode             |
 |                                                             |
 |                  +---------------+------+-------+-----------+--------------+--------------+----------------+
 |                  |               |              |           |	            |	             |                |
 |             ExprStmtNode   CompoundStmtNode   IfNode    WhileNode    ReturnStmtNode  BreakStmtNode     PrtNode
 |
 |
 +-+------------------+
 |                    |
 |                    |
 PrimitivePatNode    PatNode






Intermediate code gen is implemented for:
1) ExprStmtNode      
2) IfNode    
3) WhileNode    
4) ReturnStmtNode  
5) BreakStmtNode     
6) PrtNode
7) RefExprNode  
8) OpNode  
9) ValueNode  
10)InvocationNode
11)PatNode


Final code gen is implemented for:
1) InvocationNode
2) StmtNode (virtual)
3) CompoundStmtNode
4) RuleNode
5) ASTNode (trivial)

 ******************************************************************************/

class AstNode: public ProgramElem {
public:
  enum class NodeType {
    PAT_NODE, EXPR_NODE, REACTION_NODE, STMT_NODE, SEQUENCE_NODE, RULE_NODE
  };

  AstNode(NodeType nt, int line = 0, int column = 0, string file = "");
  AstNode(const AstNode&); // copy constructor
  NodeType nodeType() const;

  /* Virtual Functions */
  virtual ~AstNode() {}
  // Type Checking
  virtual const Type* typeCheck() { return NULL; }
  // Print type info
  virtual void typePrint(ostream& os, int indent=0) const = 0;

  virtual void print(ostream& os, int indent = 0) const = 0;

  // Final code generation
  virtual vector<Instruction*> *codeGen() { return new vector<Instruction*>(); }

  // Intermediate code generation
  virtual vector<Quadruple*> *iCodeGen() { return new vector<Quadruple*>(); }


  virtual void renameRV(string prefix) {} // new names start with given prefix

  // Operator overload
  virtual bool operator==(const AstNode&) const { return false; }
  virtual bool operator!=(const AstNode& a) const { return !operator==(a); }

private:
  NodeType nodeType_;
  const AstNode* operator=(const AstNode& other); /* disable asg */
};

inline ostream& operator<<(ostream& os, const AstNode& an) {
  an.print(os);
  return os;
}

/*****************************************************************************/

class ExprNode: public AstNode {
public:
  enum class ExprNodeType {
    REF_EXPR_NODE, OP_NODE, VALUE_NODE, INV_NODE
  };

  ExprNode(ExprNodeType et, const Value* val = 0, int line = 0, int column = 0, string file = ""); // val is saved, but not copied
  ExprNode(const ExprNode&);
  ExprNodeType exprNodeType() const;
  void exprNodeType(ExprNodeType t);
  const Type* doTypeCheck();
  const Value* value() const;
  const Type* coercedType() const;
  void coercedType(const Type* type);
  void setResultType(const Type* type);
  void setTVar(InterVar *var);
  InterVar* getTVar();
  Type* getResultType();

  /* Virtual Functions */
  virtual ~ExprNode() {}
  virtual ExprNode* clone() const=0;
  virtual vector<Quadruple*>* iCodeGen() = 0;
  virtual const Type* typeCheck() const = 0;
  void typePrint(ostream& os, int indent=0) const = 0;
  void print(ostream& os, int indent = 0) const=0;

private:
  ExprNodeType exprType_;
  const Value *val_; // reference semantics for val_ and coercedType_
  const Type* coercedType_;
  const Type* resultType_;
  InterVar *tVar_;
};

/*****************************************************************************/

class RefExprNode: public ExprNode {
public:
  RefExprNode(string ext, SymTabEntry* ste = NULL, int line = 0, int column = 0, string file = "");
  RefExprNode(const RefExprNode&);
  ~RefExprNode() {}
  ExprNode* clone() const;
  string ext() const;
  void ext(string str);
  SymTabEntry* symTabEntry() const;
  void symTabEntry(SymTabEntry *ste);
  vector<Quadruple*>* iCodeGen();
  void print(ostream& os, int indent = 0) const;
  const Type* typeCheck() const;
  void typePrint(ostream& os, int indent=0) const;

private:
  string ext_;
  SymTabEntry* sym_;
};

/*****************************************************************************/

#define MAX_OP_ARITY 2
class OpNode: public ExprNode {
public:
  enum class OpCode {
    UMINUS,
    PLUS,
    MINUS,
    MULT,
    DIV,
    MOD,
    EQ,
    NE,
    GT,
    LT,
    GE,
    LE,
    AND,
    OR,
    NOT,
    BITNOT,
    BITAND,
    BITOR,
    BITXOR,
    SHL,
    SHR,
    ASSIGN,
    PRINT,
    INVALID,
    JMP,
    JMPC,
    CALL,
    RET,
    MOVIF,
    IN,
    DEFAULT
  };

  enum class OpPrintType {
    PREFIX, INFIX, POSTFIX
  };

  struct OpInfo {
    OpCode code_;
    const char* name_;
    int arity_;
    int needParen_;
    OpPrintType prtType_;
    Type::TypeTag argType_[3];
    // operators with > 2 args can be supported
    // only if types of args k through N are identical, for 1 <= k <= 3,
    // and are given by argType[k-1]

    Type::TypeTag outType_;
    const char *typeConstraints_;
  };

  static const int VARIABLE = 100;

  OpNode(OpCode op, ExprNode *l, ExprNode *r = NULL, int line = 0, int column = 0, string file = "");
  OpNode(const OpNode&);
  ExprNode* clone() const;
  ~OpNode() {}
  OpCode opCode() const;
  const ExprNode* arg(unsigned int i) const;
  unsigned int arity() const;
  void opCode(OpCode a);
  ExprNode* arg(unsigned int i);
  vector<ExprNode*>* args();
  void print(ostream& os, int indent = 0) const;
  vector<Quadruple*>* iCodeGen();
  vector<Quadruple*>* iCodeGen(string trueLabel, string falseLabel, int flag);
  static OpCode negOpCode(OpCode opc);
  const Type* typeCheck() const;
  void typePrint(ostream& os, int indent=0) const;

private:
  unsigned int arity_;
  OpCode opCode_;
  vector<ExprNode*> arg_;
};

/*****************************************************************************/

class ValueNode: public ExprNode {
public:
  ValueNode(Value* val = 0, int line = 0, int column = 0, string file = "");
  ValueNode(const ValueNode& val);
  ExprNode* clone() const;
  ~ValueNode() {}
  vector<Quadruple*>* iCodeGen();
  void print(ostream& os, int indent = 0) const;
  const Type* typeCheck() const;
  void typePrint(ostream& os, int indent=0) const;

private:
  /* val_ field is already included in ExprNode, so no new data members */
};

/*****************************************************************************/

class InvocationNode: public ExprNode {
  // Used to represent function invocation
public:
  InvocationNode(const SymTabEntry *ste, vector<ExprNode*>* param = 0, int line = 0, int column = 0, string file = "");
  InvocationNode(const InvocationNode&);
  ExprNode* clone() const;
  ~InvocationNode() {}
  const SymTabEntry* symTabEntry() const;
  const vector<ExprNode*>* params() const;
  vector<ExprNode*>* params();
  void params(vector<ExprNode*>* args);
  const ExprNode* param(unsigned int i) const;
  ExprNode* param(unsigned int i);
  void param(ExprNode* arg, unsigned int i);
  vector<Quadruple*>* iCodeGen();
  vector<Instruction*>* codeGen();
  void print(ostream& os, int indent = 0) const;
  const Type* typeCheck() const;
  void typePrint(ostream& os, int indent=0) const;

private:
  vector<ExprNode*>* params_;
  const SymTabEntry *ste_; // reference semantics
};

/*****************************************************************************/
// There are 3 kinds of PatNodes:
//   PrimitivePatNodes are of the form: event|cond
//   AtomicPatNodes consist of PrimitivePatNodes with one or more "||"
//      operators, plus an optional negation symbol
//   PatNodes consist of PatNodes or AtomicPatNodes composed with
//      ".", "||" and "*" operators
// We have a single base class for pattern nodes called BasePatNode. In
// addition, the functionality of Atomic and PatNodes have been
// combined into a single class PatNode.
class BasePatNode: public AstNode {
public:
  enum class PatNodeKind {
    PRIMITIVE, EMPTY, NEG, SEQ, OR, STAR, UNDEFINED
  };

  const static string labelPrefix;

  BasePatNode(PatNodeKind pk, int ln, int col, string f);
  BasePatNode(const BasePatNode& bpn);
  PatNodeKind kind() const;
  void kind(PatNodeKind p);
  const BasePatNode* parent() const;
  BasePatNode* parent();

  /* Virtual Functions */
  virtual ~BasePatNode() {}
  virtual string getLabel() = 0;
  virtual vector<Quadruple*>* iCodeGen() = 0;
  virtual bool hasSeqOps() const=0;
  virtual bool hasNeg() const=0;
  virtual bool hasAnyOrOther() const=0;
  virtual bool isNegatable() const { return ((!hasSeqOps()) && (!hasNeg())); }
  virtual const Type* typeCheck() const = 0;
  virtual void typePrint(ostream& os, int indent=0) const = 0;

private:
  PatNodeKind patKind_;
  BasePatNode* parent_;
  BasePatNode* root_;
};

/*****************************************************************************/

class PrimitivePatNode: public BasePatNode {
public:
  PrimitivePatNode(EventEntry* ee, vector<VariableEntry*>* params, ExprNode* c = NULL, int line = 0, int column = 0, string file = "");
  ~PrimitivePatNode() {}

  const EventEntry* event() const;
  const vector<const VariableEntry*>* params() const;
  const ExprNode* cond() const;
  const ExprNode* condition() const;
  const list<const OpNode*>& asgs() const;
  EventEntry* event();
  vector<VariableEntry*>* params();
  ExprNode* cond();
  void cond(ExprNode* c);
  ExprNode* condition();
  list<OpNode*>& asgs();
  bool hasSeqOps() const;
  bool hasNeg() const;
  bool hasAnyOrOther() const;
  string getLabel();
  vector<Quadruple*>* iCodeGen();
  const Type* typeCheck() const;
  void typePrint(ostream& os, int indent=0) const;
  void print(ostream& os, int indent = 0) const;

private:
  EventEntry* ee_;
  vector<VariableEntry*>* params_;
  ExprNode* cond_; /* cond_ may contain assignments as well as other expressions */
  ExprNode* condition_; /* condition_ contains all expressions in cond_ other than assignments */
  list<OpNode*> asgs_;
};

/*****************************************************************************/

class PatNode: public BasePatNode {
public:
  PatNode(PatNodeKind pk, BasePatNode *p1, BasePatNode*p2 = NULL, int line = 0, int column = 0, string file = "");
  ~PatNode() {}
  const BasePatNode* pat1() const;
  BasePatNode* pat1();
  const BasePatNode* pat2() const;
  BasePatNode* pat2();
  string getLabel();
  bool hasNeg() const;
  bool hasSeqOps() const;
  bool hasAnyOrOther() const;
  void print(ostream& os, int indent = 0) const;
  const Type* typeCheck() const;
  void typePrint(ostream& os, int indent=0) const;
  vector<Quadruple*>* iCodeGen();

private:
  PatNode(const PatNode&);

  BasePatNode *pat1_;
  BasePatNode *pat2_;
};

/*****************************************************************************/

class StmtNode: public AstNode {
public:
  enum class StmtNodeKind {
    ILLEGAL = -1, EXPR, IF, COMPOUND, RETURN, WHILE, BREAK, PRINT
  };

  StmtNode(StmtNodeKind skm, int line, int column, string file);
  StmtNodeKind stmtNodeKind() const;
  vector<Quadruple*>* getICodeTable();
  void insertQuadrupleSet(vector<Quadruple *> *instrVector);
  vector<Instruction*>* fetchExprRegValue(ExprNode* expr);

  /* Virtual Functions */
  virtual ~StmtNode() {}
  virtual void print(ostream& os, int indent) const = 0;
  virtual const Type* typeCheck() const = 0;
  virtual void typePrint(ostream& os, int indent=0) const = 0;
  virtual vector<Instruction*>* codeGen() { return new vector<Instruction*>(); }

private:
  StmtNodeKind skind_;
  vector<Quadruple*>* iCodeTable_;
};

/*****************************************************************************/

class ReturnStmtNode: public StmtNode {
public:
  ReturnStmtNode(ExprNode *e, FunctionEntry* fe, int line = 0, int column = 0, string file = "");
  ~ReturnStmtNode() {}
  const ExprNode* exprNode() const;
  const FunctionEntry* funEntry() const;
  ExprNode* exprNode();
  FunctionEntry* funEntry();
  vector<Quadruple*>* iCodeGen();
  void print(ostream& os, int indent) const;
  const Type* typeCheck() const;
  void typePrint(ostream& os, int indent=0) const;

private:
  ExprNode* expr_;
  FunctionEntry* fun_;
};

/*****************************************************************************/

class ExprStmtNode: public StmtNode {
public:
  ExprStmtNode(ExprNode* e, int line = 0, int column = 0, string file = "");
  ~ExprStmtNode() {}
  const ExprNode* exprNode() const;
  ExprNode* exprNode();
  vector<Quadruple*>* iCodeGen();
  void print(ostream& os, int indent) const;
  const Type* typeCheck() const;
  void typePrint(ostream& os, int indent=0) const;

private:
  ExprNode* expr_;
};

/*****************************************************************************/

class CompoundStmtNode: public StmtNode {
public:
  CompoundStmtNode(list<StmtNode*> *Slist, int ln = 0, int col = 0, string fl = "");
  ~CompoundStmtNode() {}
  const list<StmtNode*>* stmts() const;
  vector<Instruction*>* codeGen();
  list<StmtNode*>* stmts();
  vector<Quadruple*>* iCodeGen();
  void add(StmtNode *s);
  void typePrintWithoutBraces(ostream& os, int indent) const;
  void printWithoutBraces(ostream& os, int indent) const;
  void print(ostream& os, int indent) const;
  const Type* typeCheck() const;
  void typePrint(ostream& os, int indent=0) const;

private:
  CompoundStmtNode(const CompoundStmtNode&);
  list<StmtNode*> *stmts_;
};

/*****************************************************************************/

class IfNode: public StmtNode {
public:
  IfNode(ExprNode* cond, StmtNode* thenStmt, StmtNode* elseStmt = NULL, int line = 0, int column = 0, string file = "");
  ~IfNode() {}
  const ExprNode* cond() const;
  const StmtNode* elseStmt() const;
  const StmtNode* thenStmt() const;
  ExprNode* cond();
  StmtNode* elseStmt();
  StmtNode* thenStmt();
  vector<Quadruple*>* iCodeGen();
  void print(ostream& os, int indent) const;
  const Type* typeCheck() const;
  void typePrint(ostream& os, int indent=0) const;

private:
  ExprNode *cond_;
  StmtNode *then_, *else_;
  string endLabel_;
  string elseLabel_;
  string startLabel_;
};

/*****************************************************************************/

class BreakStmtNode: public StmtNode {
public:
  BreakStmtNode(int num, BlockEntry* be, int line = 0, int column = 0, string file = "");
  ~BreakStmtNode() {}
  const int num() const;
  const BlockEntry* blockEntry() const;
  vector<Quadruple*>* iCodeGen();
  int num();
  BlockEntry* blockEntry();
  void print(ostream& os, int indent) const;
  const Type* typeCheck() const;
  void typePrint(ostream& os, int indent=0) const;

private:
  int num_;
  string brkNumLabel_;
  BlockEntry *blockEntry_;
};

/*****************************************************************************/

class WhileNode: public StmtNode {
public:
  WhileNode(ExprNode* cond, StmtNode* compStmt, string key, int line = 0, int column = 0, string file = "");
  ~WhileNode() {}
  const ExprNode* cond() const;
  const StmtNode* compStmt() const;
  ExprNode* cond();
  StmtNode* compStmt();
  vector<Quadruple*>* iCodeGen();
  void print(ostream& os, int indent) const;
  const Type* typeCheck() const;
  void typePrint(ostream& os, int indent=0) const;

private:
  ExprNode *cond_;
  StmtNode *comp_;
  string startLabel_;
  string beginLabel_;
  string endLabel_;
  string key_;

  WhileNode(const WhileNode&);
};

/*****************************************************************************/

class PrtNode: public StmtNode {

public:
  PrtNode(string prt, RefExprNode *refExpr, int ln = 0, int col = 0, string fl = "");
  ~PrtNode() {}
  RefExprNode *getRefExpr();
  string getPrt();
  void print(ostream& os, int indent) const;
  const Type* typeCheck() const;
  void typePrint(ostream& os, int indent=0) const;
  vector<Quadruple*>* iCodeGen();

private:
  RefExprNode *refExpr_;
  string prt_;
};

/*****************************************************************************/

class RuleNode: public AstNode {
public:
  RuleNode(BlockEntry *re, BasePatNode* pat, StmtNode* reaction, int line = 0, int column = 0, string file = "");
  ~RuleNode() {}
  const BlockEntry* ruleEntry() const;
  BlockEntry* ruleEntry();
  const BasePatNode* pat() const;
  BasePatNode* pat();
  const StmtNode* reaction() const;
  StmtNode* reaction();
  string getJmpName();
  void printICode();
  vector<Instruction*>* codeGen();
  void print(ostream& os, int indent = 0) const;
  const Type* typeCheck() const;
  void typePrint(ostream& os, int indent=0) const;

private:
  BlockEntry *rste_;
  BasePatNode *pat_;
  StmtNode *reaction_;
  string jmpName_;
  vector<Quadruple*>* iCodeTable_;
};

/*****************************************************************************/

#endif

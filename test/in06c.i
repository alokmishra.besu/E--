JMP begin
//
eventLabel_a: INI R012				// IN   e
ADD R006 1 R006				// PLUS z 1 z
JMP EventMStart
//
eventLabel_a: INI R012				// IN   h
MOVI 1 R006				// ASSIGN 1  z
JMP EventMStart
//
eventLabel_a: INI R012				// IN   h
MOVI 1 R006				// ASSIGN 1  z
JMP EventMStart
//
eventLabel_a: INI R012				// IN   i
MOVI R013 R006				// ASSIGN w  z
JMP EventMStart
//
eventLabel_a: INI R013				// IN   i
MOVI R014 R006				// ASSIGN w  z
JMP EventMStart
//
eventLabel_a: INI R014				// IN   j
MOVI R014 R006				// ASSIGN j  z
JMP EventMStart
//
eventLabel_a: INI R014				// IN   k
ADD R014 R011 R006				// PLUS k y z
JMP EventMStart
//
eventLabel_a: INI R014				// IN   i
MOVI R010 R006				// ASSIGN j  z
JMP EventMStart
//
begin: MOVI 10000 R000
MOVI 0 R006				// ASSIGN 0  z
MOVI 0 R007				// ASSIGN 0  f
MOVI 0 R008				// ASSIGN 0  g
MOVI 0 R009				// ASSIGN 0  w
MOVI 0 R010				// ASSIGN 0  j
MOVI 0 R011				// ASSIGN 0  y
EventMStart: IN R014
JMPC GT 64 R014 EventMOut
JMPC EQ 97 R014 eventLabel_a
JMPC EQ 97 R014 eventLabel_a
JMPC EQ 97 R014 eventLabel_a
JMPC EQ 97 R014 eventLabel_a
JMPC EQ 97 R014 eventLabel_a
JMPC EQ 97 R014 eventLabel_a
JMPC EQ 97 R014 eventLabel_a
JMPC EQ 97 R014 eventLabel_a
JMP EventMStart
EventMOut: PRTS "\nDone\n"

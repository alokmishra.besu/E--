JMP begin
//
//
Label0: ADD R000 2 R005				// Function Start-g, Setting temp register to load params
STI R001 R000				// Saving BP
SUB R000 1 R000
MOVI R000 R001				// Saving SP to BP
MOVI 1 R006				// ASSIGN 1  a1
Label1: MOVI R001 R000				// Function Exit: Restoring BP
ADD R000 1 R000
LDI R000 R001				// Loading BP from stack
ADD R000 1 R000
LDI R000 R004				// Getting Return Address from stack
ADD R000 0 R000
JMPI R004
//
eventLabel_open: INI R006				// IN   file
INI R007				// IN   flags
INI R008				// IN   mode
STI R006 R000				// Flushing Registers- file
SUB R000 1 R000
STI R007 R000				// Flushing Registers- flags
SUB R000 1 R000
STI R008 R000				// Flushing Registers- mode
SUB R000 1 R000
STI "open: testing almost nothing" R000				// Pushing parameter- "open: testing almost nothing"
SUB R000 1 R000
MOVL Label2 R004
STI R004 R000
SUB R000 1 R000
JMP
Label2: MOVI R002 R010				// Getting return Value
ADD R000 1 R000
LDI R000 R008				// Loading Back Registers:mode
ADD R000 1 R000
LDI R000 R007				// Loading Back Registers:flags
ADD R000 1 R000
LDI R000 R006				// Loading Back Registers:file
JMP EventMStart
//
eventLabel_open: INI R008				// IN   file
INI R011				// IN   flags
INI R012				// IN   mode
STI R008 R000				// Flushing Registers- file
SUB R000 1 R000
STI R011 R000				// Flushing Registers- flags
SUB R000 1 R000
STI R012 R000				// Flushing Registers- mode
SUB R000 1 R000
MOVL Label3 R004
STI R004 R000
SUB R000 1 R000
JMP Label0
Label3: ADD R000 1 R000
LDI R000 R012				// Loading Back Registers:mode
ADD R000 1 R000
LDI R000 R011				// Loading Back Registers:flags
ADD R000 1 R000
LDI R000 R008				// Loading Back Registers:file
JMP EventMStart
//
begin: MOVI 10000 R000
EventMStart: IN R012
JMPC GT 64 R012 EventMOut
JMPC EQ 111 R012 eventLabel_open
JMPC EQ 111 R012 eventLabel_open
JMP EventMStart
EventMOut: PRTS "\nDone\n"

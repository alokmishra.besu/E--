JMP begin
//
//
//
eventLabel_open: INI R008				// IN   file
INI R009				// IN   flags
INI R010				// IN   mode
STI R008 R000				// Flushing Registers- file
SUB R000 1 R000
STI R009 R000				// Flushing Registers- flags
SUB R000 1 R000
STI R010 R000				// Flushing Registers- mode
SUB R000 1 R000
STI "open: testing almost nothing" R000				// Pushing parameter- "open: testing almost nothing"
SUB R000 1 R000
MOVL Label0 R004
STI R004 R000
SUB R000 1 R000
JMP
Label0: MOVI R002 R012				// Getting return Value
ADD R000 1 R000
LDI R000 R010				// Loading Back Registers:mode
ADD R000 1 R000
LDI R000 R009				// Loading Back Registers:flags
ADD R000 1 R000
LDI R000 R008				// Loading Back Registers:file
JMP EventMStart
//
begin: MOVI 10000 R000
MOVI ErrorValue  R006				// ASSIGN ErrorValue   a1
MOVI 2 R007				// ASSIGN 2  b
EventMStart: IN R010
JMPC GT 64 R010 EventMOut
JMPC EQ 111 R010 eventLabel_open
JMP EventMStart
EventMOut: PRTS "\nDone\n"

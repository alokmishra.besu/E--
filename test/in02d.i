JMP begin
//
//
Label0: ADD R000 2 R005				// Function Start-g, Setting temp register to load params
STI R001 R000				// Saving BP
SUB R000 1 R000
MOVI R000 R001				// Saving SP to BP
MOVI 1 R018				// ASSIGN 1  a1
Label1: MOVI R001 R000				// Function Exit: Restoring BP
ADD R000 1 R000
LDI R000 R001				// Loading BP from stack
ADD R000 1 R000
LDI R000 R004				// Getting Return Address from stack
ADD R000 0 R000
JMPI R004
//
eventLabel_open: INI R018				// IN   file
INI R019				// IN   flags
INI R020				// IN   mode
STI R018 R000				// Flushing Registers- file
SUB R000 1 R000
STI R019 R000				// Flushing Registers- flags
SUB R000 1 R000
STI R020 R000				// Flushing Registers- mode
SUB R000 1 R000
STI "open: testing almost nothing" R000				// Pushing parameter- "open: testing almost nothing"
SUB R000 1 R000
MOVL Label2 R004
STI R004 R000
SUB R000 1 R000
JMP
Label2: MOVI R002 R022				// Getting return Value
ADD R000 1 R000
LDI R000 R020				// Loading Back Registers:mode
ADD R000 1 R000
LDI R000 R019				// Loading Back Registers:flags
ADD R000 1 R000
LDI R000 R018				// Loading Back Registers:file
JMP EventMStart
//
eventLabel_open: INI R020				// IN   file
INI R023				// IN   flags
INI R024				// IN   mode
STI R020 R000				// Flushing Registers- file
SUB R000 1 R000
STI R023 R000				// Flushing Registers- flags
SUB R000 1 R000
STI R024 R000				// Flushing Registers- mode
SUB R000 1 R000
MOVL Label3 R004
STI R004 R000
SUB R000 1 R000
JMP Label0
Label3: ADD R000 1 R000
LDI R000 R024				// Loading Back Registers:mode
ADD R000 1 R000
LDI R000 R023				// Loading Back Registers:flags
ADD R000 1 R000
LDI R000 R020				// Loading Back Registers:file
JMP EventMStart
//
begin: MOVI 10000 R000
MOVI ErrorValue  R006				// ASSIGN ErrorValue   x
MOVI ErrorValue  R007				// ASSIGN ErrorValue   y
MOVI 0 R008				// ASSIGN 0  i1
MOVI 0 R009				// ASSIGN 0  i2
MOVI 0 R010				// ASSIGN 0  i3
MOVF 3 F001				// ASSIGN 3  d1
MOVF ErrorValue  F002				// ASSIGN ErrorValue   d2
MOVF ErrorValue  F003				// ASSIGN ErrorValue   d3
MOVI 0 R011				// ASSIGN 0  i4
MOVI 1 R012				// ASSIGN 1  b1
MOVI 0 R013				// ASSIGN 0  b2
MOVI ErrorValue  R014				// ASSIGN ErrorValue   y2
MOVS ErrorValue  R015				// ASSIGN ErrorValue   cs1
MOVS ErrorValue  R016				// ASSIGN ErrorValue   cs2
MOVS "sdsd" R017				// ASSIGN "sdsd"  cs3
EventMStart: IN R024
JMPC GT 64 R024 EventMOut
JMPC EQ 111 R024 eventLabel_open
JMPC EQ 111 R024 eventLabel_open
JMP EventMStart
EventMOut: PRTS "\nDone\n"

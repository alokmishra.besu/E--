JMP begin
//
//
eventLabel_open: INI R006				// IN   file
INI R007				// IN   flags
INI R008				// IN   mode
STI R006 R000				// Flushing Registers- file
SUB R000 1 R000
STI R007 R000				// Flushing Registers- flags
SUB R000 1 R000
STI R008 R000				// Flushing Registers- mode
SUB R000 1 R000
STI "open: testing almost nothing" R000				// Pushing parameter- "open: testing almost nothing"
SUB R000 1 R000
MOVL Label0 R004
STI R004 R000
SUB R000 1 R000
JMP
Label0: MOVI R002 R010				// Getting return Value
ADD R000 1 R000
LDI R000 R008				// Loading Back Registers:mode
ADD R000 1 R000
LDI R000 R007				// Loading Back Registers:flags
ADD R000 1 R000
LDI R000 R006				// Loading Back Registers:file
JMP EventMStart
//
begin: MOVI 10000 R000
EventMStart: IN R008
JMPC GT 64 R008 EventMOut
JMPC EQ 111 R008 eventLabel_open
JMP EventMStart
EventMOut: PRTS "\nDone\n"

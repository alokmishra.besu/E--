JMP begin
//
eventLabel_open: INI R006				// IN   file
INI R007				// IN   flags
INI R008				// IN   mode
JMP EventMStart
//
eventLabel_any: JMP EventMStart
//
begin: MOVI 10000 R000
EventMStart: IN R008
JMPC GT 64 R008 EventMOut
JMPC EQ 111 R008 eventLabel_open
JMP eventLabel_any
JMP EventMStart
EventMOut: PRTS "\nDone\n"

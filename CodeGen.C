#include "IncludeHeaders.h"
//  Add  book keeping for the special puspose registers
//  Stack pointer: R999
//  Base Point: R998
//  Return Value: R997/F997
//  Global Vars Base Addr: R996/F996
//
//  General Purpose Registers:	returned by the global memory allocation function
//	two values: one  each for the Integer/Float registers
//
//  Register free list for tracking the registers for the temporary vars
//	: allocate from a global pool of registers
//	: add the register to used free list when not in use
//
//
//	functions required:
//	    : string fetchNextAvailReg()
//	    : void purgeReg(string regName)
//
//
//  Code Gen:
//

extern int OPTIMIZE;

// Implementation of methods for Instruction class ----------------------------------------------------------------------
const char* instrName[] = { "ADD", "SUB", "DIV", "MUL", "MOD", "FADD", "FSUB", "FDIV", "FMUL", "AND", "OR", "XOR", "NEG", "FNEG", "UGT", "UGE", "GT",
    "GE", "EQ", "NE", "FGT", "FGE", "FEQ", "FNE", "PRTS", "PRTI", "PRTF", "JMP", "JMPC", "JMPI", "JMPCI", "MOVL", "MOVS", "MOVI", "MOVF", "MOVIF",
    "MOVFI", "STI", "STF", "LDI", "LDF", "IN", "INI", "INF" };

string Instruction::name(InstructionSet inst) {
  return string(instrName[inst]);
}

Instruction* Instruction::decrSP() {
  Instruction *instr = new Instruction(Instruction::InstructionSet::SUB,
  SP_REG, "1",
  SP_REG);
  return instr;
}

string Instruction::toString(bool newLine) {
  ostringstream os;
  if (label_ != "")
    os << label_ << ": ";
  if (instr_ != InstructionSet::BLANK) {
    os << name(instr_);
    if (param1_ != "")
      os << " " << param1_;
    if (param2_ != "")
      os << " " << param2_;
    if (param3_ != "")
      os << " " << param3_;
    if (comment_ != "")
      os << "\t\t\t\t// " << comment_;
    if (newLine)
      os << endl;
  }
  return os.str();
}

// Implementation of methods for IntrCodeElem class ----------------------------------------------------------------------
bool InterVar::uses(InterVar *p) {
  if (this->equals(p)) {
    return true;
  }

  switch (type_) {
  case ElemType::QUAD_TYPE: {
    Quadruple *quad = (Quadruple*) progElem_;
    InterVar *e1 = quad->getOpr1();
    InterVar *e2 = quad->getOpr2();
    if ((e1 && e1->uses(p)) || (e2 && e2->uses(p))) {
      return true;
    }
    break;
  }
  case ElemType::PARAM_TYPE: { vector<InterVar*> *params = ((ActualParamElem*)progElem_)->getParams();
    for (auto it = params->begin(); it != params->end(); ++it) {
      if ((*it)->uses(p))
        return true;
    }
  }
    break;
  default:
    return false;
  }

  return false;
}

bool InterVar::equals(InterVar *p) {
  if (this->getElem() == p->getElem())
    return true;
  if (p->type_ != this->type_)
    return false;
  switch (this->type_) {
  case ElemType::VAL_TYPE:
    return (((ValueNode*) progElem_)->value()->toString() == ((ValueNode*) p->progElem_)->value()->toString());
  case ElemType::LABEL_TYPE:
  case ElemType::QUAD_TYPE:
  case ElemType::PARAM_TYPE:
  default:
    return false;
  }
}

string InterVar::toString(InterVar *ice) {

  if (ice == NULL) {
    return "";
  }

  ProgramElem *pe = ice->getElem();
  ElemType et = ice->getType();
  ostringstream os;

  switch (et) {
  case ElemType::VAR_TYPE:
  case ElemType::TEMP_VAR_TYPE:
    return ((VariableEntry*) pe)->name();

  case ElemType::VAL_TYPE:
    return (((ValueNode*) pe)->value())->toString();

  case ElemType::INV_NODE_TYPE:
    return (((InvocationNode*) pe)->symTabEntry())->name();

  case ElemType::REF_EXPR_TYPE:
    return ((VariableEntry*) pe)->name();

  case ElemType::QUAD_TYPE:
    return ((Quadruple*) pe)->toString(false);

  case ElemType::LABEL_TYPE:
    return ((IntrLabel*) pe)->getLabel();

  case ElemType::PARAM_TYPE:
    vector<InterVar*> *params = ((ActualParamElem*)pe)->getParams();
    for (vector<InterVar*>::iterator it = params->begin(); it != params->end(); ++it) {
      os << InterVar::toString((*it));
    }
    return os.str();
  }

  return string("");
}

/*
 * Check if given operation is associative or not,
 * only multiplication and addition are considered to
 * be associative
 */
static bool isAssociative(OpNode::OpCode opcode) {
	switch (opcode) {
  case OpNode::OpCode::MULT:
  case OpNode::OpCode::PLUS:
    return true;
  default:
    return false;
  }
}

/*
 * Check if given operation if arithmetic, i.e.
 * one of the following:
 * - Division (DIV)
 * - Substraction (MINUS)
 * - Multiplication (MULT)
 * - Addition (PLUS)
 */
static bool isArithmeticOp(OpNode::OpCode opcode) {
	switch (opcode) {
		case OpNode::OpCode::DIV:
		case OpNode::OpCode::MINUS:
		case OpNode::OpCode::MULT:
   		case OpNode::OpCode::PLUS:
			return true;
		default:
			return false;
	}
}

/*
 * Check that given operation is assignment
 */
static bool isAssignment(OpNode::OpCode opcode) {
	return (opcode == OpNode::OpCode::ASSIGN);
}

/*
 * Check that given operation is either
 * left or right bit shift
 *//*
static bool isReducible(OpNode::OpCode opcode) {
	switch (opcode) {
		case OpNode::OpCode::MULT:
		case OpNode::OpCode::DIV:
			return true;
		default:
			return false;
	}
}*/

/*
 * Function to optimize icode, which takes
 * pointer to vector of quadruples as input
 * and optimize it in place
 *
 * Implemented optimizations:
 * - Common Subexpression Elimination
 * - TEMP VARS???
 * TODO: - Strength Reduction
 */

static void doOptimization(vector<Quadruple*> *quadruples) {
	int quadrupleSize;

	// Common Subexpression Elimination
	quadrupleSize = quadruples->size();
	for (int i = 0; i < quadrupleSize - 1; i++) {
		bool isDefinition = false;
		bool isAssoc;
		Quadruple *quadruple = quadruples->at(i);

	    set<ProgramElem*> *lookupSet = new set<ProgramElem*>();

		// If we face laben we want to move to next iteration
		if (quadruple->getLabel() != "")
			continue;

		// If current quadruple is not either arithmetic operation or
		// definition then we skip that quadruple
		isDefinition = isAssignment(quadruple->getOpc());
		if ((!isArithmeticOp(quadruple->getOpc())) && (!isDefinition))
			continue;

		isAssoc = isAssociative(quadruple->getOpc());
		InterVar *fixedOpr1 = quadruple->getOpr1();
		InterVar *fixedOpr2 = quadruple->getOpr2();
		InterVar *fixedRes = quadruple->getRes();

		if (isDefinition) {
			VariableEntry *ve = (VariableEntry*)(fixedRes->getElem());
			// If the variable is global we don't remove it
			if (ve->varKind() == VariableEntry::VarKind::GLOBAL_VAR)
				continue;
		}

		for (int j = i + 1; j < quadrupleSize; j++) {
			Quadruple *quadruple2 = quadruples->at(j);
			InterVar *finalOpr1 = quadruple2->getOpr1();
			InterVar *finalOpr2 = quadruple2->getOpr2();
			InterVar *finalRes = quadruple2->getRes();

			if (isDefinition) {
				// On Jump and Conditional Jump we stop
				// TODO: we should not have JMP/JMPC in the definition
				if (quadruple2->getOpc() == OpNode::OpCode::JMP || quadruple2->getOpc() == OpNode::OpCode::JMPC)
					break;
/*
				if (finalOpr1 && finalOpr1->uses(fixedRes))
					break;
				if (finalOpr2 && finalOpr2->uses(fixedRes))
					break;
*/
				// If we already have this ops in our set we can just do substitution
				if (finalOpr1 && lookupSet->count(finalOpr1->getElem()))
					quadruple2->setOpr1(fixedRes);
				// If we already have this ops in our set we can just do substitution
				if (finalOpr2 && lookupSet->count(finalOpr2->getElem()))
					quadruple2->setOpr2(fixedRes);

				// Once we have same RHS then we erase it
				if (finalRes && finalRes->equals(fixedRes)) {
					cout << "Deletion in the definition" << endl;
					quadruples->erase(quadruples->begin() + i--);
					quadrupleSize = quadruples->size();
					break;
				}
				// If we have defition w/o fixedRes we move to next iteration
				continue;
			}
			// Again on Jump and Conditional Jump we stop
			if (quadruple2->getOpc() == OpNode::OpCode::JMP || quadruple2->getOpc() == OpNode::OpCode::JMPC)
				break;
			// If we already have this ops in our set we can just do substitution
			if (finalOpr1 && lookupSet->count(finalOpr1->getElem()))
				quadruple2->setOpr1(fixedRes);
			// If we already have this ops in our set we can just do substitution
			if (finalOpr2 && lookupSet->count(finalOpr2->getElem()))
				quadruple2->setOpr2(fixedRes);
			// On reassigning of variables we stop
			if (finalRes && (finalRes->equals(fixedOpr1) || finalRes->equals(fixedOpr2)))
				break; // Variables reassigned
			// If not arithmetic operation we move to next iteration
			if (!isArithmeticOp(quadruple2->getOpc()))
				continue;
			if (quadruple2->getOpc() == quadruple->getOpc()
								&& ((fixedOpr1->equals(finalOpr1) && fixedOpr2->equals(finalOpr2))
								|| (isAssoc && fixedOpr1->equals(finalOpr2)
								&& fixedOpr2->equals(finalOpr1)))) {
				lookupSet->insert(finalRes->getElem());
				quadruples->erase(quadruples->begin() + j--);
				quadrupleSize = quadruples->size();
			}
		}
		// Now we need to start from clean slate
		delete (lookupSet);
	}

	// Eliminating redundant temporary variables

	// Re-initialize size of quadruple set
	quadrupleSize = quadruples->size();
	for (int i = 0; i < quadrupleSize - 1; i++) {
		int iter;
		int index = -1;
		Quadruple *quadruple = quadruples->at(i);
		InterVar *res = quadruple->getRes();

		// If not temporary variable then
		// move to next iteration
		if (!res || res->getType() != InterVar::ElemType::TEMP_VAR_TYPE)
			continue;

		// Now we have fixed result so let's walk
		// through following icode
		for (iter = i + 1; iter < quadrupleSize; iter++) {
			Quadruple *quadruple2 = quadruples->at(iter);
			InterVar *opr1 = quadruple2->getOpr1();
			InterVar *opr2 = quadruple2->getOpr2();

			// Find place where is temporary variable was used
			if (isAssignment(quadruple2->getOpc()) && opr1->equals(res)) {
				if (index > -1)
					break;
        		else {
					index = iter;
					continue;
				}
			}

			// Check if this variable is actually being used
			if (opr1 && opr1->uses(res))
				break;
			// Check if this variable is actually being used
			if (opr2 && opr2->uses(res))
				break;
		}

		// Removing temporary variable if its place was found
		if (iter == quadrupleSize && index > -1) {
			Quadruple *quadruple2 = quadruples->at(index);
			quadruple->setRes(quadruple2->getRes());
			quadruples->erase(quadruples->begin() + index);
			quadrupleSize--;
		}
	}
/*
	// Strength Reduction
	quadrupleSize = quadruples->size();
	for (int i = 0; i < quadrupleSize; i++) {
		bool isReduc = false;
		int num;
		Quadruple *quadruple = quadruples->at(i);

		// If current quadruple is not either arithmetic operation or
		// definition then we skip that quadruple
		isReduc = isReducible(quadruple->getOpc());
		if (!isReduc)
			continue;

		InterCodeElem *opr2 = quadruple->getOpr2();

		num = stoi(((ValueNode*)(opr2->getElem()))->value()->toString());
		if ((num > 0 && (num & (num - 1))) == 0)
			num = (int)log2(num);
		else
			continue;

		VariableEntry *ve = (*num);
		quadruple->setOpr2(new InterCodeElem(ve, InterCodeElem::ElemType::VAR_TYPE));

		if (quadruple->getOpc() == OpNode::OpCode::MULT) {
			quadruple->setOpc(OpNode::OpCode::SHL);
		} else {
			quadruple->setOpc(OpNode::OpCode::SHR);
		}
	}
*/
	// Dead Code Elimination
	quadrupleSize = quadruples->size();
	for (int i = 0; i < quadrupleSize - 1; i++) {
		bool isDefinition = false;
		int j = i + 1;
		Quadruple *quadruple = quadruples->at(i);

		// If current quadruple is not definition then we move
		// to next iteration
		isDefinition = isAssignment(quadruple->getOpc());
		if (!isDefinition)
			continue;

		// If we face laben we want to move to next iteration
		if (quadruple->getLabel() != "")
			break;

		InterVar *res = quadruple->getRes();

		if (isDefinition) {
			VariableEntry *ve = (VariableEntry*)(res->getElem());
			// If the variable is global we don't consider it as dead
			if (ve->varKind() == VariableEntry::VarKind::GLOBAL_VAR)
				continue;
		}

		for (j = i + 1; j < quadrupleSize; j++) {
			Quadruple *quadruple2 = quadruples->at(j);
			InterVar *nextOpr1 = quadruple2->getOpr1();
			InterVar *nextOpr2 = quadruple2->getOpr2();
			InterVar *nextRes = quadruple2->getRes();

			// On Jump and Conditional Jump we stop
			if (quadruple2->getOpc() == OpNode::OpCode::JMP || quadruple2->getOpc() == OpNode::OpCode::JMPC)
				break;

			// We don't want to see labels during
			// searching for dead code
			// TODO: maybe redundant or cause incorrect behaviour
			if (quadruple->getLabel() != "")
				break;

			// If variable is used then we won't consider
			// it as dead
			if (nextOpr1 && nextOpr1->uses(res))
				break;
			if (nextOpr2 && nextOpr2->uses(res))
				break;
			if (nextRes && nextRes->uses(res))
				break;
		}

		if (j == quadrupleSize) {
			//cout << "Dead code found" << endl;
			quadruples->erase(quadruples->begin() + i--);
			quadrupleSize = quadruples->size();
		}
	}
	
}

OpCodeInstMap* OpCodeInstMap::opCodeInstMap_[] = { new OpCodeInstMap(OpNode::OpCode::UMINUS, { Instruction::InstructionSet::NEG,
    Instruction::InstructionSet::FNEG }), new OpCodeInstMap(OpNode::OpCode::PLUS, { Instruction::InstructionSet::ADD,
    Instruction::InstructionSet::FADD }), new OpCodeInstMap(OpNode::OpCode::MINUS, { Instruction::InstructionSet::SUB,
    Instruction::InstructionSet::FSUB }), new OpCodeInstMap(OpNode::OpCode::MULT, { Instruction::InstructionSet::MUL,
    Instruction::InstructionSet::FMUL }), new OpCodeInstMap(OpNode::OpCode::DIV,
    { Instruction::InstructionSet::DIV, Instruction::InstructionSet::FDIV }), new OpCodeInstMap(OpNode::OpCode::MOD, {
    Instruction::InstructionSet::MOD }), new OpCodeInstMap(OpNode::OpCode::EQ, { Instruction::InstructionSet::EQ, Instruction::InstructionSet::FEQ }),
    new OpCodeInstMap(OpNode::OpCode::NE, { Instruction::InstructionSet::NE, Instruction::InstructionSet::FNE }), new OpCodeInstMap(
        OpNode::OpCode::GT, { Instruction::InstructionSet::GT, Instruction::InstructionSet::FGT }), new OpCodeInstMap(OpNode::OpCode::LT, {
        Instruction::InstructionSet::GT, Instruction::InstructionSet::FGT }), new OpCodeInstMap(OpNode::OpCode::GE, { Instruction::InstructionSet::GE,
        Instruction::InstructionSet::FGE }), new OpCodeInstMap(OpNode::OpCode::LE,
        { Instruction::InstructionSet::GE, Instruction::InstructionSet::FGE }), new OpCodeInstMap(OpNode::OpCode::AND, {
        Instruction::InstructionSet::AND }), new OpCodeInstMap(OpNode::OpCode::OR, { Instruction::InstructionSet::OR }), new OpCodeInstMap(
        OpNode::OpCode::NOT, { Instruction::InstructionSet::XOR }), new OpCodeInstMap(OpNode::OpCode::BITNOT, { Instruction::InstructionSet::XOR }),
    new OpCodeInstMap(OpNode::OpCode::BITAND, { Instruction::InstructionSet::AND }), new OpCodeInstMap(OpNode::OpCode::BITOR, {
        Instruction::InstructionSet::OR }), new OpCodeInstMap(OpNode::OpCode::BITXOR, { Instruction::InstructionSet::XOR }), new OpCodeInstMap(
        OpNode::OpCode::SHL, { }), new OpCodeInstMap(OpNode::OpCode::SHR, { }), new OpCodeInstMap(OpNode::OpCode::ASSIGN, {
        Instruction::InstructionSet::MOVI, Instruction::InstructionSet::MOVF, Instruction::InstructionSet::MOVS }), new OpCodeInstMap(
        OpNode::OpCode::PRINT, { Instruction::InstructionSet::PRTI, Instruction::InstructionSet::PRTF, Instruction::InstructionSet::PRTS }),
    new OpCodeInstMap(OpNode::OpCode::INVALID, { }), new OpCodeInstMap(OpNode::OpCode::JMP, { Instruction::InstructionSet::JMP }), new OpCodeInstMap(
        OpNode::OpCode::JMPC, { Instruction::InstructionSet::JMPC }), new OpCodeInstMap(OpNode::OpCode::CALL, { }), new OpCodeInstMap(
        OpNode::OpCode::RET, { }), new OpCodeInstMap(OpNode::OpCode::MOVIF, { Instruction::InstructionSet::MOVIF }), new OpCodeInstMap(
        OpNode::OpCode::IN, { Instruction::InstructionSet::INI, Instruction::InstructionSet::INF }), new OpCodeInstMap(OpNode::OpCode::DEFAULT, { }) };

// Auxiliary method used in getInstructionSet() and Quaruple::iCodeToAsmGen()
// used to print out register info?
static string instructionParam(InterVar *e, vector<Instruction*> *inst_vec, bool isTarget = false) {
  if (!e) {
    return "";
  }

  switch (e->getType()) {
  case InterVar::ElemType::VAR_TYPE:
  case InterVar::ElemType::TEMP_VAR_TYPE:
  case InterVar::ElemType::REF_EXPR_TYPE: {
    VariableEntry *ve = (VariableEntry*) (e->getElem());
    return regMgr->getVEReg(ve, inst_vec, isTarget);
  }
  case InterVar::ElemType::VAL_TYPE: {
    ValueNode *vn = (ValueNode*) (e->getElem());
    return vn->value()->toString();
  }
    break;
  case InterVar::ElemType::INV_NODE_TYPE:
  case InterVar::ElemType::PARAM_TYPE:
    throw -1;
    //Control Should not reach here, so some error
  case InterVar::ElemType::QUAD_TYPE: {
    vector<Quadruple*> *temp = new vector<Quadruple*>();
    temp->push_back((Quadruple*) (e->getElem()));
    return Quadruple::iCodeToAsmGen(temp, false, false)->at(0)->toString(false);
  }
  case InterVar::ElemType::LABEL_TYPE:
    return ((IntrLabel*) (e->getElem()))->getLabel();
  }

  return "";
}

// Auxiliary method used in Quadruples::iCodeToAsmGen()
static vector<Instruction*>* getInstructionSet(OpNode::OpCode opc, InterVar *e1, InterVar *e2, InterVar *e3, string label,
    string comment) {
  int instNum = 0;
  vector<Instruction*> *inst_vec = new vector<Instruction*>();
  if (e3) {
    Type *inst_type = e3->getElem()->type();
    instNum = Type::isFloat(inst_type->tag()) ? 1 : 0;
  }
  string param1 = "", param2 = "", param3 = "";

  param1 = instructionParam(e1, inst_vec);
  param2 = instructionParam(e2, inst_vec);
  param3 = instructionParam(e3, inst_vec, true);

  switch (opc) {
  case OpNode::OpCode::LT:
  case OpNode::OpCode::LE:
    swap(e1, e2);
  case OpNode::OpCode::GT:
  case OpNode::OpCode::GE: {
    Type *inst_type = e1->getElem()->type();
    instNum = Type::isFloat(inst_type->tag()) ? 1 : 0;
  }
    break;
  case OpNode::OpCode::ASSIGN: {
    Type *inst_type = e3->getElem()->type();
    instNum = Type::isFloat(inst_type->tag()) ? 1 : 0;
    if (Type::isString(inst_type->tag()))
      instNum = 2;
  }
    break;
  case OpNode::OpCode::PRINT: {
    Type *inst_type = e1->getElem()->type();
    instNum = Type::isFloat(inst_type->tag()) ? 1 : 0;
    if (Type::isString(inst_type->tag()))
      instNum = 2;
  }
    break;
  case OpNode::OpCode::SHL: {
    inst_vec->push_back(new Instruction(Instruction::InstructionSet::MOVI, param1, param3, "", "", "SHL start: " + comment));
    string label1 = regMgr->getNextLabel();
    string label2 = regMgr->getNextLabel();
    string tempReg = regMgr->fetchNextAvailReg(true);
    inst_vec->push_back(new Instruction(Instruction::InstructionSet::MOVI, param2, tempReg));
    inst_vec->push_back(new Instruction(Instruction::InstructionSet::MOVI, "1", TEMP_REG));
    inst_vec->push_back(new Instruction(Instruction::InstructionSet::JMPC, "GT " + string(TEMP_REG) + " " + tempReg, label2, "", label1));
    inst_vec->push_back(new Instruction(Instruction::InstructionSet::MUL, param3, "2", param3));
    inst_vec->push_back(new Instruction(Instruction::InstructionSet::ADD, "1", TEMP_REG, TEMP_REG));
    inst_vec->push_back(new Instruction(Instruction::InstructionSet::JMP, label1, "", "", "", "SHL Ends"));
    inst_vec->push_back(new Instruction(label2));
    regMgr->purgeReg(tempReg);
    return inst_vec;
  }
  case OpNode::OpCode::SHR: {
    inst_vec->push_back(new Instruction(Instruction::InstructionSet::MOVI, param1, param3, "", "", "SHL start: " + comment));
    string label1 = regMgr->getNextLabel();
    string label2 = regMgr->getNextLabel();
    string tempReg = regMgr->fetchNextAvailReg(true);
    inst_vec->push_back(new Instruction(Instruction::InstructionSet::MOVI, param2, tempReg));
    inst_vec->push_back(new Instruction(Instruction::InstructionSet::MOVI, "1", TEMP_REG));
    inst_vec->push_back(new Instruction(Instruction::InstructionSet::JMPC, "GT " + string(TEMP_REG) + " " + tempReg, label2, "", label1));
    inst_vec->push_back(new Instruction(Instruction::InstructionSet::DIV, param3, "2", param3));
    inst_vec->push_back(new Instruction(Instruction::InstructionSet::ADD, "1", TEMP_REG, TEMP_REG));
    inst_vec->push_back(new Instruction(Instruction::InstructionSet::JMP, label1, "", "", "", "SHL Ends"));
    inst_vec->push_back(new Instruction(label2));
    regMgr->purgeReg(tempReg);
    return inst_vec;
  }
  default:
    break;
  }

  param1 = instructionParam(e1, inst_vec);
  param2 = instructionParam(e2, inst_vec);
  param3 = instructionParam(e3, inst_vec, true);
  if (isArithmeticOp(opc)) {
    if (instNum == 1) {
      string reg = regMgr->fetchNextAvailReg(false);
      if (param1.c_str()[0] == 'R') {
        inst_vec->push_back(new Instruction(Instruction::InstructionSet::MOVIF, param1, reg));
        param1 = reg;
      } else if (param1.c_str()[0] != 'F') {
        if (param1.find(".") == string::npos)
          param1 = param1 + ".0";
      }
      string reg1 = regMgr->fetchNextAvailReg(false);
      if (param2.c_str()[0] == 'R') {
        inst_vec->push_back(new Instruction(Instruction::InstructionSet::MOVIF, param2, reg1));
        param2 = reg1;
      } else if (param2.c_str()[0] != 'F') {
        if (param2.find(".") == string::npos)
          param2 = param2 + ".0";
      }
    }
  }

  Instruction::InstructionSet instCode = OpCodeInstMap::fetchInstr(opc, instNum);
  Instruction *inst = new Instruction(instCode, param1, param2, param3, label, comment);
  inst_vec->push_back(inst);
  return inst_vec;
}

static void insertIntoSet(InterVar* e, set<VariableEntry*> *entrySet) {
  if (e == NULL)
    return;
  switch (e->getType()) {
  case InterVar::ElemType::VAR_TYPE:
  case InterVar::ElemType::TEMP_VAR_TYPE:
  case InterVar::ElemType::REF_EXPR_TYPE:
    entrySet->insert((VariableEntry*) (e->getElem()));
    break;
  default:
    break;
  }

}

// Implementation of methods for Quadruple class ----------------------------------------------------------------------
const char* opCodeName[] = { "UMINUS", "PLUS", "MINUS", "MULT", "DIV", "MOD", "EQ", "NE", "GT", "LT", "GE", "LE", "AND", "OR", "NOT", "BITNOT",
    "BITAND", "BITOR", "BITXOR", "SHL", "SHR", "ASSIGN", "PRINT", "INVALID", "JMP", "JMPC", "CALL", "RET", "MOVIF", "IN", "DEFAULT" };

string Quadruple::toString(bool newLine) {
  ostringstream os;
  string param1 = "", param2 = "", param3 = "";
  string opc = opCodeName[(int) opc_];

  param1 = InterVar::toString(opr1_);
  param2 = InterVar::toString(opr2_);
  param3 = InterVar::toString(res_);

  if (label_ != "") {
    os << label_ << ": ";
  }

  os << opc << " " << param1 << " " << param2 << " " << param3;

  if (newLine) {
    os << endl;
  }

  return os.str();
}

int Quadruple::tempCnt_ = 0;

string Quadruple::fetchTempVar() {
  ostringstream os;
  os << "T" << tempCnt_++;
  return os.str();
}

void Quadruple::resetTempCnt() {
  tempCnt_ = 0;
}

bool Quadruple::isEqual(Quadruple *quad) {
  /*
   if(quad->opc_ == opc_) {
   if(quad->opr1_->name().compare(opr1_->name()) == 0) {
   if(quad->opr2_->name().compare(opr2_->name()) ==0) {
   return true;
   }
   }
   }
   */
  return false;
}

vector<Instruction*>* Quadruple::iCodeToAsmGen(vector<Quadruple*> *quad, bool showComment, bool purgeRegisters) {

  // Optimize Intermediate code before code gen
  if(OPTIMIZE)
    doOptimization(quad);

  InterVar *ve1, *ve2, *ve3;
  string label = "";
  set<VariableEntry*> *entrySet = new set<VariableEntry*>();
  vector<Instruction*> *inst_set = new vector<Instruction*>();
  vector<Instruction*> *instructionSet;
  OpNode::OpCode opc;
  for (vector<Quadruple*>::iterator it = quad->begin(); it != quad->end(); ++it) {

    opc = (*it)->getOpc();
    ve1 = (*it)->getOpr1();
    ve2 = (*it)->getOpr2();
    ve3 = (*it)->getRes();
    label = (*it)->getLabel();

    // Handle the CALL command seperately
    if (opc == OpNode::OpCode::CALL) {
      std::set<VariableEntry*>::iterator it;
      string reg = instructionParam(ve3, inst_set, true);
      for (it = entrySet->begin(); it != entrySet->end(); ++it) {
        VariableEntry *ve = (*it);
        if (ve->getReg() == "")
          continue;
        if (ve->varKind() == VariableEntry::VarKind::GLOBAL_VAR && ve->isMem()) {
          regMgr->purgeReg(ve->getReg(), inst_set);
          continue;
        } else if (ve->varKind() == VariableEntry::VarKind::GLOBAL_VAR && !ve->isMem())
          continue;
        bool isFloat = Type::isFloat(ve->type()->tag());
        inst_set->push_back(
            new Instruction(isFloat ? Instruction::InstructionSet::STF : Instruction::InstructionSet::STI, ve->getReg(), SP_REG, "", "",
                "Flushing Registers- " + ve->name()));
        inst_set->push_back(Instruction::decrSP());
      }
      InvocationNode *in = (InvocationNode*) (ve1->getElem());

      vector<InterVar*> *params = ((ActualParamElem*)(ve2->getElem()))->getParams();
      for (vector<InterVar*>::reverse_iterator it = params->rbegin(); it != params->rend(); ++it) {
        string val = instructionParam((*it), inst_set);
        insertIntoSet((*it), entrySet);
        bool isFloat = Type::isFloat((*it)->getElem()->type()->tag());
        inst_set->push_back(
            new Instruction(isFloat ? Instruction::InstructionSet::STF : Instruction::InstructionSet::STI, val, SP_REG, "", "",
                "Pushing parameter- " + InterVar::toString((*it))));
        inst_set->push_back(Instruction::decrSP());
      }

      concatVector(inst_set, in->codeGen());
      if (ve3) {
        VariableEntry *retVe = (VariableEntry*) (ve3->getElem());
        bool isFloat = Type::isFloat(retVe->type()->tag());
        inst_set->push_back(
            new Instruction(isFloat ? Instruction::InstructionSet::MOVF : Instruction::InstructionSet::MOVI, isFloat ? RETF_REG : RETI_REG,
                retVe->getReg(), "", "", "Getting return Value"));
      }
      std::set<VariableEntry*>::reverse_iterator rit;
      for (rit = entrySet->rbegin(); rit != entrySet->rend(); ++rit) {
        VariableEntry *ve = (*rit);
        if (ve->getReg() == "")
          continue;
        if (ve->varKind() == VariableEntry::VarKind::GLOBAL_VAR && !ve->isMem())
          continue;
        bool isFloat = Type::isFloat(ve->type()->tag());
        inst_set->push_back(new Instruction(Instruction::InstructionSet::ADD, SP_REG, "1", SP_REG));
        inst_set->push_back(new Instruction(isFloat ? Instruction::InstructionSet::LDF : Instruction::InstructionSet::LDI,
        SP_REG, ve->getReg(), "", "", "Loading Back Registers:" + ve->name()));
      }
      continue;
    }

    insertIntoSet(ve1, entrySet);
    insertIntoSet(ve2, entrySet);
    insertIntoSet(ve3, entrySet);

    // Handle RET and DEFAULT seperately
    switch (opc) {
    case OpNode::OpCode::RET: {
      string reg = instructionParam(ve1, inst_set);
      VariableEntry *ve = (VariableEntry*) (ve1->getElem());
      bool isFloat = Type::isFloat(ve->type()->tag());
      inst_set->push_back(
          new Instruction(isFloat ? Instruction::InstructionSet::MOVF : Instruction::InstructionSet::MOVI, reg, isFloat ? RETF_REG : RETI_REG, "",
              label));
      continue;
    }
    case OpNode::OpCode::DEFAULT:
      inst_set->push_back(new Instruction(instructionParam(ve1, inst_set)));
      continue;
    default:
      break;
    }

    // Common to all Quandruples
    instructionSet = getInstructionSet(opc, ve1, ve2, ve3, label, showComment ? (*it)->toString(false) : "");
    concatVector(inst_set, instructionSet);
    delete (instructionSet);
  }
  if (purgeRegisters) {
    Quadruple::resetTempCnt();
    std::set<VariableEntry*>::iterator it;
    for (it = entrySet->begin(); it != entrySet->end(); ++it) {
      VariableEntry *ve = (*it);
      if (ve->getReg() == "")
        continue;
      if (ve->varKind() == VariableEntry::VarKind::GLOBAL_VAR && !ve->isMem())
        continue;
      regMgr->purgeReg(ve->getReg(), inst_set);
    }
  }
  return inst_set;
}
